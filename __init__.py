# -*- coding: utf-8 -*-
import telebot as tb
import traceback
import datetime
from config import token
import requests
from messages import message_start, \
    message_help, \
    message_unknown_error, \
    message_remove_to_start, \
    message_think_up, \
    message_start_create_event, \
    message_type_event, \
    message_error_type, \
    message_format_time, \
    message_input_time,\
    message_input_date,\
    message_format_date,\
    message_input_location,\
    message_error_location,\
    message_input_description,\
    message_result_create_event,\
    message_find_date,\
    message_unknown_event_user,\
    message_find_type,\
    message_find_location,\
    message_find_radius,\
    message_error_radius,\
    message_empty_find,\
    message_result_find,\
    message_error_id_event,\
    message_empty_your_actual_event,\
    message_your_actual_event,\
    message_error_remove_event,\
    message_input_new_coordinates,\
    message_input_new_date,\
    message_input_new_description,\
    message_input_new_time,\
    message_input_new_title,\
    message_input_new_type,\
    message_error_set_user_to_event_type,\
    message_input_coordinates_check_in,\
    message_big_length,\
    message_not_check_in

from emoji import add, \
    list_event, \
    find, \
    notebook, \
    profile, \
    sos, \
    back,\
    one,\
    two,\
    three,\
    four,\
    five,\
    check_in,\
    check_mark,\
    cross_mark,\
    question,\
    end_flag,\
    miscellaneous
from db.__init__ import registration_user, \
    create_event, \
    get_types_event, \
    set_type_to_event,\
    set_time_to_event,\
    set_date_to_event,\
    set_location_to_event,\
    set_description_to_event,\
    delete_event,\
    find_events,\
    get_event,\
    get_event_today_user_go_maybe_go,\
    get_my_actual_event,\
    get_event_user,\
    remove_event_user,\
    set_title_to_event,\
    set_type_user_to_event,\
    check_in_user,\
    set_opinion,\
    get_inf_user
from db.redis_ import set_last_step_user, \
    get_last_step_user, \
    set_new_event_id, \
    set_step_create_event, \
    get_step_create_event, \
    get_new_event_id,\
    remove_event_id,\
    set_date_find_user,\
    set_location_find_user,\
    set_type_find_user,\
    get_date_find_user,\
    get_location_find_user,\
    get_type_find_user,\
    set_step_find_event,\
    get_step_find_event,\
    set_radius_find,\
    get_radius_find_user,\
    set_location_event_today,\
    set_radius_event_today,\
    set_step_event_today,\
    set_type_event_today,\
    get_location_event_today,\
    get_radius_event_today,\
    get_step_event_today,\
    get_type_event_today,\
    set_step_actual_event_user,\
    get_step_actual_event_user,\
    set_actual_event_id,\
    get_actual_event_id,\
    set_actual_event_type_user,\
    get_actual_event_type_user,\
    set_id_current_event,\
    get_id_current_event
#from celery import Celery
#from config import path_celery
from datetime import timedelta
from celery_task import send_data
time_l = 86400


bot = tb.TeleBot(token)

user_markup_default = tb.types.ReplyKeyboardMarkup(False, True, True)
user_markup_default.row(add, list_event, find)
user_markup_default.row(notebook)# , end_flag)
user_markup_default.row(profile, sos)


@bot.message_handler(commands=['start'])
def handler_text(message):
    registration_user(message.chat.first_name,
                      message.chat.last_name,
                      message.from_user.id)

    # user_markup = tb.types.ReplyKeyboardMarkup(True, True, True)
    # user_markup.row('➕', '📃', '✖')
    # user_markup.row('❓')
    # bot.send_location(message.from_user.id,
    #                  50.027453,
    #                  36.223717,
    #                  reply_markup=user_markup)

    # CAADAgADQgADGgZFBJvaLJpbpAXhAg
    bot.send_sticker(message.from_user.id,
                     'CAADAgADQgADGgZFBJvaLJpbpAXhAg')
    bot.send_message(message.from_user.id,
                     message_start.format(message.chat.first_name),
                     reply_markup=user_markup_default)
    set_last_step_user(message.from_user.id, 0)
    # bot.send_voice(message.from_user.id, 'text',reply_markup=user_markup)


@bot.message_handler(content_types=['location'])
def handler_text(message):
    # from pprint import pprint
    # pprint(str(message))
    step = get_last_step_user(message.from_user.id)
    # print(step)
    if step == 1:
        date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
        date_markup.row(back)
        try:
            step_create_event = get_step_create_event(message.from_user.id)
            # print(step_create_event)
            if step_create_event == 4:

                id_event = get_new_event_id(message.from_user.id)
                # print((message.location.latitude)) # longitude
                # print((message.location.longitude))
                res = requests.get('https://maps.googleapis.com/maps/api/geocode/json?latlng={},'
                                   '{}&key=AIzaSyAtdcwr5Dojv-zdFs3IMs1Cl2vAFkpoj08&language=ru'
                                   .format(message.location.latitude,
                                           message.location.longitude)).json()
                address = res['results'][0]['formatted_address']
                res = set_location_to_event(message.from_user.id,
                                            message.location.latitude,
                                            message.location.longitude,
                                            address,
                                            id_event)
                if res >= 0:
                    set_step_create_event(message.from_user.id,
                                          5)
                    bot.send_message(message.from_user.id,
                                     message_input_description,
                                     reply_markup=date_markup)
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_location,
                                     reply_markup=date_markup)
        except:
            traceback.print_exc()
            bot.send_message(message.from_user.id,
                             message_error_location,
                             reply_markup=date_markup)
    # print(message.location)

    if step == 2:
        step_find = get_step_find_event(message.from_user.id)
        if step_find == 2:
            set_location_find_user(message.from_user.id,
                                   message.location.latitude,
                                   message.location.longitude,)
            radius_markup = tb.types.ReplyKeyboardMarkup(False, True)
            radius_markup.row(one,
                              three,
                              five)
            radius_markup.row(back)
            set_step_find_event(message.from_user.id,
                                3)
            traceback.print_exc()
            bot.send_message(message.from_user.id,
                             message_find_radius,
                             reply_markup=radius_markup)

    if step == 3:
        step_find = get_step_event_today(message.from_user.id)
        if step_find == 0:
            set_location_event_today(message.from_user.id,
                                     message.location.latitude,
                                     message.location.longitude)
            radius_markup = tb.types.ReplyKeyboardMarkup(False, True)
            radius_markup.row(one,
                              three,
                              five)
            radius_markup.row(back)
            set_step_event_today(message.from_user.id,
                                 1)
            traceback.print_exc()
            bot.send_message(message.from_user.id,
                             message_find_radius,
                             reply_markup=radius_markup)
        if step_find == -1:
            # print(12435)
            lat = message.location.latitude
            lon = message.location.longitude
            id_event = get_id_current_event(message.from_user.id)
            tomorrow = datetime.datetime.utcnow() + timedelta(minutes=1)

            code_s = check_in_user(message.from_user.id,
                                   id_event,
                                   lat,
                                   lon)
            date = datetime.datetime.now()
            date_ = '{}-{}-{}'.format(date.month, date.day, date.year)
            radius = get_radius_event_today(message.from_user.id)
            location = get_location_event_today(message.from_user.id)
            type_ = get_type_event_today(message.from_user.id)
            # показывать события не меняя клаву


            res = find_events(date_,
                              type_,
                              location,
                              radius,
                              message.from_user.id)
            keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
            code, text, location = get_event(id_event,
                                             message.from_user.id)
            # print(code_s)
            if code_s < 0:

                if code_s == -2:
                    bot.send_message(message.from_user.id,
                                     message_big_length)
                else:
                    bot.send_message(message.from_user.id,
                                     message_not_check_in)
            else:
                # print('__')
                send_data.delay(id_event=id_event, id_user=message.from_user.id)
                # print('__')
            if code == 0:


                # print(1)
                if not location[2]:
                    keyboard.row(check_mark, question, cross_mark)
                if code_s < 0:
                    keyboard.row(check_in)
                for r in res:
                    keyboard.row(r)
                    # keyboard.add(url_button)
                # set_last_step_user(message.from_user.id, 0)
                keyboard.row(back)
                bot.send_message(message.from_user.id,
                                 text, parse_mode='Markdown',
                                 reply_markup=keyboard)
                bot.send_location(message.from_user.id, location[0], location[1])

    if step == 4:
        # print('sdfasdf')
        type_up = int(get_actual_event_type_user(message.from_user.id))
        keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
        id_event = get_actual_event_id(message.from_user.id)

        try:
            if type_up == 5:

                res = requests.get('https://maps.googleapis.com/maps/api/geocode/json?latlng={},'
                                   '{}&key=AIzaSyAtdcwr5Dojv-zdFs3IMs1Cl2vAFkpoj08&language=ru'
                                   .format(message.location.latitude,
                                           message.location.longitude)).json()
                address = res['results'][0]['formatted_address']
                res = set_location_to_event(message.from_user.id,
                                            message.location.latitude,
                                            message.location.longitude,
                                            address,
                                            id_event)
                if res >= 0:
                    pass
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_location)
        except:
            traceback.print_exc()
            bot.send_message(message.from_user.id,
                             message_error_location)

        set_step_actual_event_user(message.from_user.id, 1)
        code, text, location = get_event_user(id_event,
                                              message.from_user.id)
        keyboard.row(miscellaneous + 'название', miscellaneous + 'тип')
        keyboard.row(miscellaneous + 'дата', miscellaneous + 'время')
        keyboard.row(miscellaneous + 'координаты', miscellaneous + 'описание')
        keyboard.row(cross_mark)
        keyboard.row(back)
        bot.send_message(message.from_user.id,
                         text, parse_mode='Markdown',
                         reply_markup=keyboard)
        bot.send_location(message.from_user.id,
                          location[0],
                          location[1])


@bot.message_handler(content_types=['sticker'])
def handler_text(message):
    print(message.sticker)


@bot.message_handler(content_types=['text'])
def handler_text(message):
    #tb.types.MessageEntity('url',0,100, url='https://uk.wikipedia.org/wiki/LOL')
    #print(message)
    # bot.send_message(message.from_user.id, 'df',entities=tb.types.MessageEntity('url', 0, 100, url='https://uk.wikipedia.org/wiki/LOL'))

    def create_event_bot():
        step_create_event = get_step_create_event(message.from_user.id)
        if step_create_event == 0:
            # название
            id_event = create_event(message.from_user.id, message.text)
            if id_event < 1:
                traceback.print_exc()
                bot.send_message(message.from_user.id,
                                 message_unknown_error.format(message.chat.first_name))
                bot.send_sticker(message.from_user.id,
                                 'CAADAgADWQUAAhhC7gjAh_XnjWItWgI')
            else:
                set_new_event_id(message.from_user.id,
                                 id_event)
                set_step_create_event(message.from_user.id,
                                      1)  # перешли к выбору типа
                types = get_types_event()
                type_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                for t in types:
                    type_markup.row(t)
                type_markup.row(back)
                bot.send_message(message.from_user.id,
                                 message_type_event,
                                 reply_markup=type_markup)

        elif step_create_event == 1:
            # type event
            id_event = get_new_event_id(message.from_user.id)
            res = set_type_to_event(message.from_user.id, message.text, id_event)
            date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
            date_markup.row(back)
            if res >= 0:
                # to time
                set_step_create_event(message.from_user.id,
                                      2)
                bot.send_message(message.from_user.id,
                                 message_input_time,
                                 reply_markup=date_markup)
            else:
                bot.send_message(message.from_user.id,
                                 message_error_type,
                                 reply_markup=date_markup)

        elif step_create_event == 2:
            time = message.text
            hh, mm = int(time.split('-')[0]), int(time.split('-')[1])
            if hh >= 0 and hh <= 23 and mm >= 0 and mm <= 59:
                id_event = get_new_event_id(message.from_user.id)
                res = set_time_to_event(message.from_user.id, time, id_event)
                if res >= 0:
                    set_step_create_event(message.from_user.id,
                                          3)
                    date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)

                    date = datetime.datetime.now()
                    list_date = ['{}-{}-{}'.format(date.month, date.day, date.year)]
                    for i in range(13):
                        date += datetime.timedelta(days=1)
                        list_date.append('{}-{}-{}'.format(date.month, date.day, date.year))
                    for i in range(0, 14, 2):
                        date_markup.row(list_date[i],
                                        list_date[i + 1])
                    date_markup.row(back)

                    bot.send_message(message.from_user.id,
                                     message_input_date,
                                     reply_markup=date_markup)
                else:
                    bot.send_message(message.from_user.id,
                                     message_format_time)
            else:
                bot.send_message(message.from_user.id,
                                 message_format_time)

        elif step_create_event == 3:
            date_ = message.text
            # date create list and answer created date
            id_event = get_new_event_id(message.from_user.id)
            date = datetime.datetime.now()
            list_date = ['{}-{}-{}'.format(date.month, date.day, date.year)]
            for _ in range(13):
                date += datetime.timedelta(days=1)
                list_date.append('{}-{}-{}'.format(date.month, date.day, date.year))
            is_exist_date = False
            for dat in list_date:
                if dat == date_:
                    is_exist_date = True
                    break
            if is_exist_date:
                res = set_date_to_event(message.from_user.id,
                                        message.text,
                                        id_event)

                if res >= 0:
                    date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                    date_markup.row(back)
                    set_step_create_event(message.from_user.id,
                                          4)
                    bot.send_message(message.from_user.id,
                                     message_input_location.format(message.chat.first_name),
                                     reply_markup=date_markup)

                else:
                    date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                    for i in range(0, 14, 2):
                        date_markup.row(list_date[i],
                                        list_date[i + 1])
                    date_markup.row(back)

                    bot.send_message(message.from_user.id,
                                     message_format_date.format(message.chat.first_name),
                                     reply_markup=date_markup)
            else:
                date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                for i in range(0, 14, 2):
                    date_markup.row(list_date[i],
                                    list_date[i + 1])
                date_markup.row(back)

                bot.send_message(message.from_user.id,
                                 message_format_date.format(message.chat.first_name),
                                 reply_markup=date_markup)

        elif step_create_event == 5:
            # description
            id_event = get_new_event_id(message.from_user.id)
            set_description_to_event(message.from_user.id,
                                     message.text,
                                     id_event)
            remove_event_id(message.from_user.id)
            set_last_step_user(message.from_user.id, 0)
            bot.send_message(message.from_user.id,
                             message_result_create_event,
                             reply_markup=user_markup_default)

    def find_event_bot():

        step_find = get_step_find_event(message.from_user.id)
        if step_find == 0:
            date_ = message.text
            date = datetime.datetime.now()
            list_date = ['{}-{}-{}'.format(date.month, date.day, date.year)]
            for i in range(13):
                date += datetime.timedelta(days=1)
                list_date.append('{}-{}-{}'.format(date.month, date.day, date.year))
            is_exist_date = False
            for dat in list_date:
                if dat == date_:
                    is_exist_date = True
                    break
            if is_exist_date:
                set_date_find_user(message.from_user.id, date_)
                set_step_find_event(message.from_user.id, 1)
                types = get_types_event()
                type_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                for t in types:
                    type_markup.row(t)
                type_markup.row(back)
                bot.send_message(message.from_user.id,
                                 message_find_type,
                                 reply_markup=type_markup)

            else:
                date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                for i in range(0, 14, 2):
                    date_markup.row(list_date[i],
                                    list_date[i + 1])
                date_markup.row(back)

                bot.send_message(message.from_user.id,
                                 message_format_date.format(message.chat.first_name),
                                 reply_markup=date_markup)

        elif step_find == 1:
            set_type_find_user(message.from_user.id, message.text)
            set_step_find_event(message.from_user.id, 2)
            bot.send_message(message.from_user.id,
                             message_find_location,
                             reply_markup=tb.types.ReplyKeyboardMarkup(False, True, True).row(back))
        elif step_find == 3:
            radius = 3
            if message.text == one:
                radius = 1
            elif message.text == three:
                radius = 3
            elif message.text == five:
                radius = 5
            else:

                bot.send_message(message.from_user.id,
                                 message_error_radius.format(message.chat.first_name),
                                 reply_markup=tb.types.ReplyKeyboardMarkup(False, True, True).row(back))
            set_radius_find(message.from_user.id, radius)
            location = get_location_find_user(message.from_user.id)
            date_ = get_date_find_user(message.from_user.id)
            type_ = get_type_find_user(message.from_user.id)
            res = find_events(date_,
                              type_,
                              location,
                              radius,
                              message.from_user.id)
            if res:
                keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
                for r in res:
                    keyboard.row(r)
                    # keyboard.add(url_button)
                # set_last_step_user(message.from_user.id, 0)
                keyboard.row(back)
                set_step_find_event(message.from_user.id, 4)
                bot.send_message(message.from_user.id,
                                 message_result_find.format(message.chat.first_name),
                                 reply_markup=keyboard)

            else:
                set_last_step_user(message.from_user.id, 0)
                bot.send_message(message.from_user.id,
                                 message_empty_find.format(message.chat.first_name),
                                 reply_markup=user_markup_default)
        else:
            is_ok = False
            if message.text == check_mark:
                # go1
                id_event = get_id_current_event(message.from_user.id)
                res = set_type_user_to_event(id_event, message.from_user.id, 1)
                if res >= 0:
                    is_ok = True
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_set_user_to_event_type)
                    # return
            elif message.text == question:

                id_event = get_id_current_event(message.from_user.id)
                res = set_type_user_to_event(id_event, message.from_user.id, 3)

                if res >= 0:
                    is_ok = True
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_set_user_to_event_type)
                    # return
            elif message.text == cross_mark:
                # not go2
                id_event = get_id_current_event(message.from_user.id)
                res = set_type_user_to_event(id_event, message.from_user.id, 2)
                if res >= 0:
                    is_ok = True
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_set_user_to_event_type)
                    # return
            radius = get_radius_find_user(message.from_user.id)
            location = get_location_find_user(message.from_user.id)
            date_ = get_date_find_user(message.from_user.id)
            type_ = get_type_find_user(message.from_user.id)
            # показывать события не меняя клаву

            res = find_events(date_,
                              type_,
                              location,
                              radius,
                              message.from_user.id)
            keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
            try:
                if not is_ok:
                    id_event = message.text.split('.')[0]
                    set_id_current_event(message.from_user.id,
                                         id_event)
                else:
                    id_event = get_id_current_event(message.from_user.id)
                    # print(id_event)
                code, text, location = get_event(id_event,
                                                 message.from_user.id)
                if code == 0:
                    if not location[2]:
                        keyboard.row(check_mark, question, cross_mark)
                    # keyboard.row(check_in)
                    for r in res:
                        keyboard.row(r)
                        # keyboard.add(url_button)
                    # set_last_step_user(message.from_user.id, 0)
                    keyboard.row(back)
                    bot.send_message(message.from_user.id,
                                     text,
                                     parse_mode='Markdown',
                                     reply_markup=keyboard)
                    bot.send_location(message.from_user.id, location[0], location[1])

                elif code == -1:
                    for r in res:
                        keyboard.row(r)
                        # keyboard.add(url_button)
                    # set_last_step_user(message.from_user.id, 0)
                    keyboard.row(back)
                    bot.send_sticker(message.from_user.id,
                                     'CAADAgADWAADGgZFBOgvy03gQOGgAg')
                    bot.send_message(message.from_user.id,
                                     message_error_id_event.format(message.chat.first_name),
                                     reply_markup=keyboard)
                else:
                    for r in res:
                        keyboard.row(r)
                        # keyboard.add(url_button)
                    # set_last_step_user(message.from_user.id, 0)
                    keyboard.row(back)
                    bot.send_sticker(message.from_user.id,
                                     'CAADAgADigADGgZFBHF3_01Pcn2FAg')
                    bot.send_message(message.from_user.id,
                                     message_error_id_event.format(message.chat.first_name),
                                     reply_markup=keyboard)
            except:
                # pass
                traceback.print_exc()
                for r in res:
                    keyboard.row(r)
                    # keyboard.add(url_button)
                # set_last_step_user(message.from_user.id, 0)
                keyboard.row(back)
                bot.send_sticker(message.from_user.id,
                                 'CAADAgADWAADGgZFBOgvy03gQOGgAg')
                bot.send_message(message.from_user.id,
                                 message_error_id_event.format(message.chat.first_name),
                                 reply_markup=keyboard)
                # CAADAgADWAADGgZFBOgvy03gQOGgAg
            pass

    def event_today():

        date = datetime.datetime.now()
        date_ = '{}-{}-{}'.format(date.month, date.day, date.year)
        step_event_today = get_step_event_today(message.from_user.id)
        if step_event_today == 1:
            radius = 3
            if message.text == one:
                radius = 1
            elif message.text == three:
                radius = 3
            elif message.text == five:
                radius = 5
            else:

                bot.send_message(message.from_user.id,
                                 message_error_radius.format(message.chat.first_name),
                                 reply_markup=tb.types.ReplyKeyboardMarkup(False, True, True).row(back))
            set_radius_event_today(message.from_user.id, radius)
            set_step_event_today(message.from_user.id, 2)
            types = get_types_event()
            type_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
            for t in types:
                type_markup.row(t)
            type_markup.row(back)
            bot.send_message(message.from_user.id,
                             message_find_type,
                             reply_markup=type_markup)
        elif step_event_today == 2:
            type_ = message.text
            set_type_event_today(message.from_user.id, type_)
            location = get_location_event_today(message.from_user.id)
            radius = get_radius_event_today(message.from_user.id)
            res = find_events(date_,
                              type_,
                              location,
                              radius,
                              message.from_user.id)
            if res:
                keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
                for r in res:
                    keyboard.row(r)
                    # keyboard.add(url_button)
                # set_last_step_user(message.from_user.id, 0)
                keyboard.row(back)
                set_step_event_today(message.from_user.id, 3)
                bot.send_message(message.from_user.id,
                                 message_result_find.format(message.chat.first_name),
                                 reply_markup=keyboard)

            else:
                set_last_step_user(message.from_user.id,0)
                bot.send_message(message.from_user.id,
                                 message_empty_find.format(message.chat.first_name),
                                 reply_markup=user_markup_default)
        else:
            # print(message.text)
            is_ok = False
            if message.text == check_mark:
                # go1
                id_event = get_id_current_event(message.from_user.id)
                res = set_type_user_to_event(id_event, message.from_user.id, 1)
                if res >= 0:
                    is_ok = True
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_set_user_to_event_type)
                # return
            elif message.text == question:

                id_event = get_id_current_event(message.from_user.id)
                res = set_type_user_to_event(id_event, message.from_user.id, 3)

                if res >= 0:
                    is_ok = True
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_set_user_to_event_type)
                # return
            elif message.text == cross_mark:
                # not go2
                id_event = get_id_current_event(message.from_user.id)
                res = set_type_user_to_event(id_event, message.from_user.id, 2)
                if res >= 0:
                    is_ok = True
                else:
                    bot.send_message(message.from_user.id,
                                     message_error_set_user_to_event_type)
                # return
            elif message.text == check_in:
                bot.send_message(message.from_user.id,
                                 message_input_coordinates_check_in)
                set_step_event_today(message.from_user.id, -1)
                return


            radius = get_radius_event_today(message.from_user.id)
            location = get_location_event_today(message.from_user.id)
            type_ = get_type_event_today(message.from_user.id)
            # показывать события не меняя клаву

            res = find_events(date_,
                              type_,
                              location,
                              radius,
                              message.from_user.id)
            keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
            # print(is_ok)
            try:
                if not is_ok:
                    id_event = message.text.split('.')[0]
                    set_id_current_event(message.from_user.id,
                                         id_event)
                else:
                    id_event = get_id_current_event(message.from_user.id)
                # print(id_event)
                code, text, location = get_event(id_event,
                                                 message.from_user.id)
                if code == 0:

                    # print(1)
                    if not location[2]:
                        keyboard.row(check_mark, question, cross_mark)
                    if not location[3]:
                        keyboard.row(check_in)
                    for r in res:
                        keyboard.row(r)
                        # keyboard.add(url_button)
                    # set_last_step_user(message.from_user.id, 0)
                    keyboard.row(back)
                    bot.send_message(message.from_user.id,
                                     text, parse_mode='Markdown',
                                     reply_markup=keyboard)
                    bot.send_location(message.from_user.id, location[0], location[1])

                elif code == -1:
                    for r in res:
                        keyboard.row(r)
                        # keyboard.add(url_button)
                    # set_last_step_user(message.from_user.id, 0)
                    keyboard.row(back)
                    bot.send_sticker(message.from_user.id,
                                     'CAADAgADWAADGgZFBOgvy03gQOGgAg')
                    bot.send_message(message.from_user.id,
                                     message_error_id_event.format(message.chat.first_name),
                                     reply_markup=keyboard)
                else:
                    for r in res:
                        keyboard.row(r)
                        # keyboard.add(url_button)
                    # set_last_step_user(message.from_user.id, 0)
                    keyboard.row(back)
                    bot.send_sticker(message.from_user.id,
                                     'CAADAgADigADGgZFBHF3_01Pcn2FAg')
                    bot.send_message(message.from_user.id,
                                     message_error_id_event.format(message.chat.first_name),
                                     reply_markup=keyboard)
            except:
                keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
                # pass
                traceback.print_exc()
                for r in res:
                    keyboard.row(r)
                    # keyboard.add(url_button)
                # set_last_step_user(message.from_user.id, 0)
                keyboard.row(back)
                bot.send_sticker(message.from_user.id,
                                 'CAADAgADWAADGgZFBOgvy03gQOGgAg')
                bot.send_message(message.from_user.id,
                                 message_error_id_event.format(message.chat.first_name),
                                 reply_markup=keyboard)
                # CAADAgADWAADGgZFBOgvy03gQOGgAg

    try:
        step = get_last_step_user(message.from_user.id)
        # print(message.text)
        if message.text == '🔙':  # '🔙 '
            # print(1)
            if step == 1:
                id_event = get_new_event_id(message.from_user.id)
                delete_event(message.from_user.id,
                             id_event)
            set_last_step_user(message.from_user.id, 0)
            bot.send_message(message.from_user.id,
                             message_remove_to_start.format(message.chat.first_name),
                             reply_markup=user_markup_default)
        else:
            if step == 0:
                # смотреть какой символ прислал
                if message.text == add:
                    set_last_step_user(message.from_user.id, 1)
                    bot.send_message(message.from_user.id,
                                     message_start_create_event.format(message.chat.first_name))
                    bot.send_message(message.from_user.id,
                                     message_think_up.format(message.chat.first_name),
                                     reply_markup=tb.types.ReplyKeyboardMarkup(False, True, True).row('🔙'))
                    set_step_create_event(message.from_user.id, 0)

                elif message.text == find:
                    set_last_step_user(message.from_user.id, 2)
                    date = datetime.datetime.now()
                    date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                    list_date = ['{}-{}-{}'.format(date.month, date.day, date.year)]
                    for i in range(13):
                        date += datetime.timedelta(days=1)
                        list_date.append('{}-{}-{}'.format(date.month, date.day, date.year))
                    for i in range(0, 14, 2):
                        date_markup.row(list_date[i],
                                        list_date[i + 1])
                    set_step_find_event(message.from_user.id, 0)
                    date_markup.row(back)
                    bot.send_message(message.from_user.id,
                                     message_find_date.format(message.chat.first_name),
                                     reply_markup=date_markup)

                elif message.text == list_event:
                    set_last_step_user(message.from_user.id, 3)
                    res = get_event_today_user_go_maybe_go(message.from_user.id)
                    today_ev = tb.types.ReplyKeyboardMarkup(False, True, True)
                    for r in res:
                        today_ev.row(r)
                    today_ev.row(back)
                    set_step_event_today(message.from_user.id, 0)
                    bot.send_message(message.from_user.id,
                                     message_find_location.format(message.chat.first_name),
                                     reply_markup=today_ev)
                elif message.text == sos:
                    bot.send_sticker(message.from_user.id,
                                     'CAADAgADegADGgZFBLEHj54VZGIgAg')
                    bot.send_message(message.from_user.id,
                                     message_help.format(message.chat.first_name))
                elif message.text == notebook:
                    # туточки все твои предстоящие события
                    res = get_my_actual_event(message.from_user.id)
                    if res:
                        set_last_step_user(message.from_user.id, 4)
                        set_step_actual_event_user(message.from_user.id, 0)
                        user_actual_event = tb.types.ReplyKeyboardMarkup(False, True, True)
                        for r in res:
                            user_actual_event.row(r)
                        user_actual_event.row(back)
                        bot.send_message(message.from_user.id,
                                         message_your_actual_event.format(message.chat.first_name),
                                         reply_markup=user_actual_event)

                    else:
                        bot.send_message(message.from_user.id,
                                         message_empty_your_actual_event.format(message.chat.first_name),
                                         reply_markup=user_markup_default)

                    pass
                #elif message.text == end_flag:
                #    # а туточки какие прошли
                #    pass
                elif message.text == profile:
                    te = get_inf_user(message.from_user.id)
                    bot.send_message(message.from_user.id,te,reply_markup=user_markup_default)
                else:
                    bot.send_sticker(message.from_user.id,
                                     'CAADAgADVAADGgZFBHo9RTRiHBZ-Ag')
                    bot.send_message(message.from_user.id,
                                     message_unknown_event_user.format(message.chat.first_name),
                                     reply_markup=user_markup_default)

            elif step == -1:
                op = 3
                if message.text == one:
                    op = 1
                elif message.text == two:
                    op = 2
                elif message.text == three:
                    op = 3
                elif message.text == four:
                    op = 4
                elif message.text == five:
                    op = 5
                id_event = get_id_current_event(message.from_user.id)
                set_opinion(id_event, message.from_user.id, op)
                bot.send_message(message.from_user.id,
                                 message_remove_to_start.format(message.chat.first_name),
                                 reply_markup=user_markup_default)
                set_last_step_user(message.from_user.id, 0)
            elif step == 1:
                create_event_bot()
            elif step == 2:
                find_event_bot()
            elif step == 3:
                event_today()
            elif step == 4:
                step_ac = get_step_actual_event_user(message.from_user.id)
                if step_ac == 0:
                    # переходим в выбранную инициативу
                    keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
                    try:
                        id_event = message.text.split('.')[0]
                        code, text, location = get_event_user(id_event,
                                                              message.from_user.id)
                        if code == 0:
                            set_actual_event_id(message.from_user.id, id_event)
                            set_step_actual_event_user(message.from_user.id,1)
                            # print(1)
                            keyboard.row(miscellaneous + 'название', miscellaneous + 'тип')
                            keyboard.row(miscellaneous + 'дата', miscellaneous + 'время')
                            keyboard.row(miscellaneous + 'координаты', miscellaneous + 'описание')
                            keyboard.row(cross_mark)
                            keyboard.row(back)
                            bot.send_message(message.from_user.id,
                                             text, parse_mode='Markdown',
                                             reply_markup=keyboard)
                            bot.send_location(message.from_user.id,
                                              location[0],
                                              location[1])

                        elif code == -1:
                            keyboard.row(back)
                            bot.send_sticker(message.from_user.id,
                                             'CAADAgADWAADGgZFBOgvy03gQOGgAg')
                            bot.send_message(message.from_user.id,
                                             message_error_id_event.format(message.chat.first_name))
                        elif code == -3:
                            keyboard.row(back)
                            bot.send_sticker(message.from_user.id,
                                             'CAADAgADigADGgZFBHF3_01Pcn2FAg')
                            bot.send_message(message.from_user.id,
                                             message_error_id_event.format(message.chat.first_name))
                        else:
                            keyboard.row(back)
                            bot.send_sticker(message.from_user.id,
                                             'CAADAgADigADGgZFBHF3_01Pcn2FAg')
                            bot.send_message(message.from_user.id,
                                             message_error_id_event.format(message.chat.first_name))
                    except:
                        # pass
                        traceback.print_exc()
                        keyboard.row(back)
                        bot.send_sticker(message.from_user.id,
                                         'CAADAgADWAADGgZFBOgvy03gQOGgAg')
                        bot.send_message(message.from_user.id,
                                         message_error_id_event.format(message.chat.first_name))
                        # CAADAgADWAADGgZFBOgvy03gQOGgAg
                elif step_ac == 1:
                    # print(message.text)
                    # в зависимости от того что выбрал то и делаем
                    if message.text == cross_mark:
                        id_event = get_actual_event_id(message.from_user.id)
                        code = remove_event_user(id_event, message.from_user.id)
                        if code == 0:
                            res = get_my_actual_event(message.from_user.id)
                            if res:
                                set_last_step_user(message.from_user.id, 4)
                                set_step_actual_event_user(message.from_user.id, 1)
                                user_actual_event = tb.types.ReplyKeyboardMarkup(False, True, True)
                                for r in res:
                                    user_actual_event.row(r)
                                user_actual_event.row(back)
                                bot.send_message(message.from_user.id,
                                                 message_your_actual_event.format(message.chat.first_name),
                                                 reply_markup=user_actual_event)

                            else:
                                set_last_step_user(message.from_user.id, 0)
                                bot.send_message(message.from_user.id,
                                                 message_empty_your_actual_event.format(message.chat.first_name),
                                                 reply_markup=user_markup_default)
                        else:
                            bot.send_sticker(message.from_user.id,
                                             'CAADAgADdAADGgZFBNo-_BU12Uo9Ag')
                            bot.send_message(message.from_user.id,
                                             message_error_remove_event.format(message.chat.first_name))
                    elif message.text == miscellaneous + 'название':
                        set_step_actual_event_user(message.from_user.id, 2)
                        set_actual_event_type_user(message.from_user.id, 1)
                        bot.send_message(message.from_user.id,
                                         message_input_new_title.format(message.chat.first_name))
                    elif message.text == miscellaneous + 'тип':
                        set_step_actual_event_user(message.from_user.id, 2)
                        set_actual_event_type_user(message.from_user.id, 2)
                        types = get_types_event()
                        type_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                        for t in types:
                            type_markup.row(t)
                        type_markup.row(back)
                        bot.send_message(message.from_user.id,
                                         message_input_new_type.format(message.chat.first_name),
                                         reply_markup=type_markup)
                    elif message.text == miscellaneous + 'дата':
                        set_step_actual_event_user(message.from_user.id, 2)
                        set_actual_event_type_user(message.from_user.id, 3)
                        date = datetime.datetime.now()
                        # date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                        list_date = ['{}-{}-{}'.format(date.month, date.day, date.year)]
                        for i in range(13):
                            date += datetime.timedelta(days=1)
                            list_date.append('{}-{}-{}'.format(date.month, date.day, date.year))
                        is_exist_date = False
                        date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                        for i in range(0, 14, 2):
                            date_markup.row(list_date[i],
                                            list_date[i + 1])
                        date_markup.row(back)
                        bot.send_message(message.from_user.id,
                                         message_input_new_date.format(message.chat.first_name),
                                         reply_markup=date_markup)
                    elif message.text == miscellaneous + 'время':
                        set_step_actual_event_user(message.from_user.id, 2)
                        set_actual_event_type_user(message.from_user.id, 4)
                        bot.send_message(message.from_user.id,
                                         message_input_new_time.format(message.chat.first_name))
                    elif message.text == miscellaneous + 'координаты':
                        set_step_actual_event_user(message.from_user.id, 2)
                        set_actual_event_type_user(message.from_user.id, 5)
                        bot.send_message(message.from_user.id,
                                         message_input_new_coordinates.format(message.chat.first_name))
                    elif message.text == miscellaneous + 'описание':
                        set_step_actual_event_user(message.from_user.id, 2)
                        set_actual_event_type_user(message.from_user.id, 6)
                        bot.send_message(message.from_user.id,
                                         message_input_new_description.format(message.chat.first_name))
                else:
                    # print('asdasdasdasd')
                    type_up = int(get_actual_event_type_user(message.from_user.id))
                    keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
                    id_event = get_actual_event_id(message.from_user.id)

                    if type_up == 1:
                        set_title_to_event(message.from_user.id,
                                           message.text,
                                           id_event)
                    elif type_up == 2:
                        set_type_to_event(message.from_user.id,
                                          message.text,
                                          id_event)
                    elif type_up == 3:
                        date = datetime.datetime.now()
                        list_date = ['{}-{}-{}'.format(date.month, date.day, date.year)]
                        for i in range(13):
                            date += datetime.timedelta(days=1)
                            list_date.append('{}-{}-{}'.format(date.month, date.day, date.year))
                        is_exist_date = False
                        date_markup = tb.types.ReplyKeyboardMarkup(False, True, True)
                        date_ = message.text
                        for dat in list_date:
                            if dat == date_:
                                is_exist_date = True
                                break
                        if is_exist_date:
                            set_date_to_event(message.from_user.id,
                                              date_,
                                              id_event)
                        else:
                            bot.send_message(message.from_user,
                                             message_format_date)
                            # return
                            # date_markup.row(back)
                    elif type_up == 4:
                        time = message.text
                        try:
                            hh, mm = int(time.split('-')[0]), int(time.split('-')[1])
                            if hh >= 0 and hh <= 23 and mm >= 0 and mm <= 59:
                                set_time_to_event(message.from_user.id,
                                                  message.text,
                                                  id_event)
                            else:
                                bot.send_message(message.from_user.id,
                                                 message_format_time)
                        except:
                            bot.send_message(message.from_user.id,
                                             message_format_time)
                            # return
                        # time
                    elif type_up == 6:
                        set_description_to_event(message.from_user.id, message.text, id_event)
                    else:
                        pass
                        # сказать что ничего не сделал
                    set_step_actual_event_user(message.from_user.id, 1)
                    code, text, location = get_event_user(id_event,
                                                          message.from_user.id)
                    keyboard.row(miscellaneous + 'название', miscellaneous + 'тип')
                    keyboard.row(miscellaneous + 'дата', miscellaneous + 'время')
                    keyboard.row(miscellaneous + 'координаты', miscellaneous + 'описание')
                    keyboard.row(cross_mark)
                    keyboard.row(back)
                    bot.send_message(message.from_user.id,
                                     text, parse_mode='Markdown',
                                     reply_markup=keyboard)
                    bot.send_location(message.from_user.id,
                                      location[0],
                                      location[1])
    except:
        traceback.print_exc()
        bot.send_sticker(message.from_user.id,
                         'CAADAgADigADGgZFBHF3_01Pcn2FAg')
        bot.send_message(message.from_user.id,
                         message_unknown_error.format(message.chat.first_name))
        # data = {
        #     'width': 512,
        #     'emoji': '😨',
        #     'thumb': {
        #         'width': 128,
        #         'file_id': 'AAQCABPgIbcNAARBkYM1_6CETescAAIC',
        #         'file_size': 6586,
        #         'height': 128
        #     },
        #     'file_id': 'CAADAgADWQUAAhhC7gjAh_XnjWItWgI',
        #     'file_size': 38142,
        #     'height': 512}


