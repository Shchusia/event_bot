CREATE DATABASE tele_bot_event;
CREATE EXTENSION postgis;
CREATE EXTENSION btree_gist;

CREATE SEQUENCE user_to_event_type_id START 1;
CREATE SEQUENCE user_to_event_id START 1;
CREATE SEQUENCE user_to_event_opinion_id START 1;

CREATE SEQUENCE users_id START 1;
CREATE SEQUENCE event_type_id START 1;
CREATE SEQUENCE event_id START 1;

CREATE SEQUENCE logger_step_user_id START 1;
CREATE TABLE users(
  id_user INT PRIMARY KEY DEFAULT nextval('users_id'),
  first_name VARCHAR(100),
  last_name VARCHAR(100),
  chat_id VARCHAR(20) UNIQUE ,
  created_on timestamp without time zone DEFAULT timezone('utc'::text, now())
);

CREATE TABLE event_type(
  id_event_type INT PRIMARY KEY DEFAULT nextval('event_type_id'),
  type_event VARCHAR(100)
);
INSERT INTO event_type (id_event_type, type_event) VALUES (0,'прочее');

CREATE TABLE events(
  id_event INT PRIMARY KEY DEFAULT nextval('event_id'),
  title_event VARCHAR(200),
  ref_id_type_event INT REFERENCES event_type(id_event_type),
  text_event_type VARCHAR(100),
  location GEOMETRY(POINT, 4326),
  address_event VARCHAR(200),
  time_event  time,
  date_event date,
  description VARCHAR(1200),
  ref_id_users INT REFERENCES users(id_user)
);



CREATE TABLE user_to_event_type(
  id_user_to_event_type INT PRIMARY KEY DEFAULT nextval('user_to_event_type_id'),
  type_ VARCHAR(100)
);

CREATE TABLE user_to_event(
  id_user_to_event INT UNIQUE DEFAULT nextval('user_to_event_id'),
  ref_id_user INT REFERENCES users (id_user),
  ref_id_event INT REFERENCES events(id_event),
  ref_user_to_event_type INT REFERENCES user_to_event_type(id_user_to_event_type),
  PRIMARY KEY(ref_id_event, ref_id_user)
);

CREATE TABLE user_to_event_opinion(
  id_user_to_event_opinion INT PRIMARY KEY DEFAULT nextval('user_to_event_opinion_id'),
  opinion INT,
  ref_id_user_to_event INT REFERENCES user_to_event (id_user_to_event)
);

