import redis
import traceback
import datetime

r = redis.StrictRedis(host='localhost', port=6379, db=1)


def set_last_step_user(chat_id, step):
    # 0 - start
    # 1 - создание события
    # 3 - получение списка сегодняшних
    # 2 - поиск предстоящих событий
    # 4 - обо мне
    r.set(chat_id, step)


def get_last_step_user(chat_id):
    try:
        return int(r.get(chat_id).decode('utf-8'))
    except KeyError:
        return 0


def set_step_create_event(chat_id, step):
    r.set('new_event_{}'.format(chat_id), step)


def get_step_create_event(chat_id):
    try:
        return int(r.get('new_event_{}'.format(chat_id)).decode('utf-8'))
    except KeyError:
        return 0


def set_new_event_id(chat_id, id_event):
    r.set('new_event_{}_id'.format(chat_id),id_event)


def get_new_event_id(chat_id):
    try:
        return int(r.get('new_event_{}_id'.format(chat_id)).decode('utf-8'))
    except:
        return 0


def remove_event_id(chat_id):
    try:
        r.delete('new_event_{}_id'.format(chat_id))
    except:
        pass


def is_exist_user(chat_id):
    try:
        if not r.get(chat_id) is None:
            return True
        return False
    except:
        return False


def delete_user(chat_id):
    try:
        r.delete(chat_id)
    except:
        pass


def set_step_find_event(chat_id, step):
    r.set('find_{}'.format(chat_id), step)


def get_step_find_event(chat_id):
    try:
        return int(r.get('find_{}'.format(chat_id)).decode('utf-8'))
    except KeyError:
        return 0


def set_date_find_user(chat_id, date_):
    r.set('find_date_{}'.format(chat_id), date_)


def get_date_find_user(chat_id):
    return r.get('find_date_{}'.format(chat_id)).decode('utf-8')


def set_type_find_user(chat_id, type_):
    r.set('find_type_{}'.format(chat_id), type_)


def get_type_find_user(chat_id):
    return r.get('find_type_{}'.format(chat_id)).decode('utf-8')


def set_location_find_user(chat_id, lat, lng):
    r.set('find_location_{}'.format(chat_id), '{} {}'.format(lat, lng))


def get_location_find_user(chat_id):
    return r.get('find_location_{}'.format(chat_id)).decode('utf-8')


def set_radius_find(chat_id, radius):
    r.set('find_radius_{}'.format(chat_id), radius)


def get_radius_find_user(chat_id):
    return r.get('find_radius_{}'.format(chat_id)).decode('utf-8')


def set_step_event_today(chat_id, step):
    r.set('find_{}'.format(chat_id), step)


def get_step_event_today(chat_id):
    try:
        return int(r.get('find_{}'.format(chat_id)).decode('utf-8'))
    except KeyError:
        return 0


def set_type_event_today(chat_id, type_):
    r.set('find_type_{}'.format(chat_id), type_)


def get_type_event_today(chat_id):
    return r.get('find_type_{}'.format(chat_id)).decode('utf-8')


def set_location_event_today(chat_id, lat, lng):
    r.set('find_location_{}'.format(chat_id), '{} {}'.format(lat, lng))


def get_location_event_today(chat_id):
    return r.get('find_location_{}'.format(chat_id)).decode('utf-8')


def set_radius_event_today(chat_id, radius):
    r.set('find_radius_{}'.format(chat_id), radius)


def get_radius_event_today(chat_id):
    return r.get('find_radius_{}'.format(chat_id)).decode('utf-8')


def set_step_actual_event_user(chat_id, step):
    r.set('actual_{}'.format(chat_id), step)


def get_step_actual_event_user(chat_id):
    try:
        return int(r.get('actual_{}'.format(chat_id)).decode('utf-8'))
    except KeyError:
        return 0


def set_actual_event_id(chat_id, id_event):
    r.set('actual_{}_id'.format(chat_id), id_event)


def get_actual_event_id(chat_id):
    try:
        return int(r.get('actual_{}_id'.format(chat_id)).decode('utf-8'))
    except:
        return 0


def set_actual_event_type_user(chat_id, type_):
    r.set('actual_{}_'.format(chat_id), type_)


def get_actual_event_type_user(chat_id):
    try:
        return int(r.get('actual_{}_'.format(chat_id)).decode('utf-8'))
    except:
        return 0


def set_id_current_event(chat_id, id_event):
    r.set('cur_{}'.format(chat_id), id_event)


def get_id_current_event(chat_id):
    try:
        return int(r.get('cur_{}'.format(chat_id)).decode('utf-8'))
    except:
        return 0