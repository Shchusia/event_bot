import psycopg2
from config import str_connect_to_db
import hashlib,\
    datetime,\
    binascii,\
    os
import traceback
from db.answer_db import error_connect,\
    error_select,\
    step_to_log_added
import datetime


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except:
        print("I am unable to connect to the database")
    return conn


def get_id_user_by_chat_id(chat_id, cursor):
    select = '''
        SELECT id_user
        FROM users
        WHERE chat_id = '{}'
    '''.format(chat_id)
    cursor.execute(select)
    # print(select)
    return cursor.fetchone()[0]


def registration_user(first_name, last_name, chat_id):
    conn = create_connection()
    if conn:
        try:
            select = '''
            INSERT INTO users (id_user, first_name, last_name, chat_id, created_on)
            VALUES (DEFAULT , '{}', '{}', '{}', DEFAULT);
            '''.format(first_name, last_name, chat_id)
            cur = conn.cursor()
            cur.execute(select)
            conn.commit()
            cur.close()
            conn.close()
            return ''
        except:
            #   traceback.print_exc()
            conn.close()
            return ''
    return ""


def get_types_event():
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        select = '''
            SELECT type_event
            FROM event_type 
            WHERE id_event_type > 0
        '''
        cur.execute(select)
        rows = [r[0] for r in cur.fetchall()]
        conn.close()
        return rows
    return []


def create_event(chat_id,
                 title_event):
    select = '''
        INSERT INTO events (id_event, title_event, ref_id_users) 
        VALUES (DEFAULT , '{}', {}) returning id_event
    '''
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            cur.execute(select.format(title_event.replace("'", "''"), id_user))
            id_event = cur.fetchone()[0]
            conn.commit()
            conn.close()
            return id_event
        except:
            traceback.print_exc()
            conn.close()
            return 0
    return 0


def get_id_type_event(type_event,
                      cur):
    select = '''
        SELECT id_event_type
        FROM event_type
        WHERE type_event = '{}'
    '''.format(type_event.replace("'", "''"))
    cur.execute(select)
    row = cur.fetchone()
    if row:
        return row[0]
    return 0


def set_type_to_event(chat_id,
                      type_event,
                      id_event):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_type = get_id_type_event(type_event, cur)
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                UPDATE events
                SET ref_id_type_event = {},
                text_event_type = '{}'
                WHERE id_event = {}
                AND ref_id_users = {}
            '''.format(id_type,
                       type_event.replace("'", "''"),
                       id_event,id_user)
            cur.execute(select)
            # rows = cur.fetchone()
            conn.commit()
            conn.close()
            #if rows > 0:
            return 0
            #return -1
        except:
            traceback.print_exc()
            conn.close()
            return -1
    return -1


def set_title_to_event(chat_id,
                       title_event,
                       id_event):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            # id_type = get_id_type_event(type_event, cur)
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                UPDATE events
                SET title_event = '{}'
                WHERE id_event = {}
                AND ref_id_users = {}
            '''.format(title_event.replace("'", "''"),
                       id_event,id_user)
            cur.execute(select)
            # rows = cur.fetchone()
            conn.commit()
            conn.close()
            #if rows > 0:
            return 0
            #return -1
        except:
            traceback.print_exc()
            conn.close()
            return -1
    return -1


def set_time_to_event(chat_id,
                      time_event,
                      id_event):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                UPDATE events
                SET time_event = '{}'
               WHERE id_event = {}
                AND ref_id_users = {}
            '''.format(time_event.replace('-',':'),
                       id_event,
                       id_user)
            cur.execute(select)
            #rows = cur.fetchone()[0]
            conn.commit()
            conn.close()
            #if rows > 0:
            return 0
            return -1
        except:
            traceback.print_exc()
            conn.close()
            return -1
    return -1


def set_date_to_event(chat_id,
                      date_event,
                      id_event):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                UPDATE events
                SET date_event = '{}'
               WHERE id_event = {}
                AND ref_id_users = {}
            '''.format(date_event.replace(':','-'),
                       id_event,
                       id_user)
            cur.execute(select)
            # rows = cur.fetchone()[0]
            conn.commit()
            conn.close()
            #if rows > 0:
            return 0
            return -1
        except:
            traceback.print_exc()
            conn.close()
            return -1
    return -1


def set_location_to_event(chat_id,
                          lat,
                          lng,
                          address,
                          id_event):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                UPDATE events
                SET location = 'SRID=4326;POINT({} {})',
                address_event = '{}'
                WHERE id_event = {}
                AND ref_id_users = {}
            '''.format(lat, lng,
                       address,
                       id_event,
                       id_user)
            cur.execute(select)
            #rows = cur.fetchone()[0]
            conn.commit()
            conn.close()
            #if rows > 0:
            return 0
            return -1
        except:
            traceback.print_exc()
            conn.close()
            return -1
    return -1


def set_description_to_event(chat_id,
                             desctription,
                             id_event):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                UPDATE events
                SET description = '{}'
                WHERE id_event = {}
                AND ref_id_users = {}
            '''.format(desctription,
                       id_event,
                       id_user)
            cur.execute(select)
            #rows = cur.fetchone()[0]
            conn.commit()
            conn.close()
            #if rows > 0:
            return 0
            return -1
        except:
            traceback.print_exc()
            conn.close()
            return -1
    return -1


def delete_event(chat_id,
                 id_event):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_user = get_id_user_by_chat_id(chat_id, cur)
            select = '''
                   DELETE FROM events WHERE id_event = {} AND ref_id_users = {}
               '''.format(id_event,
                          id_user)
            cur.execute(select)
            # rows = cur.fetchone()[0]
            conn.commit()
            conn.close()
            # if rows > 0:
            return 0
            return -1
        except:
            traceback.print_exc()
            conn.close()
            return -1
    return -1


def find_events(date_,
                type_,
                location,
                radius,
                chat_id):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_type = get_id_type_event(type_, cur)
            select = '''
                   SELECT e.id_event, e.title_event, e.time_event,ST_Distance(e.location::geography, 'SRID=4326;POINT({})'::geography)/1000 as dist_km
                   FROM  events e
                   WHERE e.ref_id_type_event = {}
                   AND e.date_event = '{}'
                   AND ST_Distance(e.location::geography, 'SRID=4326;POINT({})'::geography)/1000 < {}
                   ORDER BY e.location <-> 'SRID=4326;POINT({})';
               '''.format(location,
                          id_type,
                          date_,
                          location,
                          radius,
                          location)
            # print(select)
            cur.execute(select)
            rows = cur.fetchall()
            res = []
            conn.close()
            for r in rows:
                if id_type == 0:
                    s = '{}. {} в {} ({} от вас) ({})'.format(r[0], r[1], r[2], round(r[3], 3), type_)
                else:
                    s = '{}. {} в {} ({} от вас)'.format(r[0], r[1], r[2], round(r[3], 3))
                res.append(s)
            return res
        except:
            traceback.print_exc()
            pass
            return []
    return []


def is_exist_event(id_event, cur):
    select = '''
    SELECT id_event
    FROM events
    WHERE id_event = {}
    '''.format(id_event)
    cur.execute(select)
    if cur.fetchone():
        return True
    return False


def get_user(id_user, cur):
    select = '''
        SELECT first_name, last_name
        FROM users
        WHERE id_user = {}
    '''.format(id_user)

    cur.execute(select)
    row = cur.fetchone()
    # print(row)
    if row[1] != 'None':
        return '{} {}'.format(row[0], row[1])
    return row[0]


def get_range_event_user(id_user,
                         cur):
    select = '''
        SELECT count(ueo.id_user_to_event_opinion), sum(ueo.opinion)
        FROM events e, user_to_event ue, user_to_event_opinion ueo
        WHERE e.id_event = ue.ref_id_event
        AND e.ref_id_users = {}
        AND ueo.ref_id_user_to_event = ue.id_user_to_event
    '''.format(id_user)
    cur.execute(select)
    row = cur.fetchone()
    if row[0] == 0:
        return 'еще нет оценок'
    return round(float(row[1])/row[0],2)


def get_event(id_event,
              id_user=None):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        is_type_us_to_ev = False
        is_check = False
        if id_user:
            id_u = get_id_user_by_chat_id(id_user,
                                          cur)
            select = '''
                SELECT ref_user_to_event_type
                FROM user_to_event
                WHERE ref_id_event = {} AND ref_id_user = {}
            '''.format(id_event,id_u)
            cur.execute(select)
            try:
                row = cur.fetchone()
                if row:
                    if row[0] == 4:
                        is_check = True
                    is_type_us_to_ev = True
            except:
                pass
        if is_exist_event(id_event, cur):
            select = '''
                SELECT e.title_event, e.text_event_type, e.ref_id_type_event, e.time_event, e.date_event,
                e.description, e.ref_id_users,
                ST_x(ST_Transform( e.location, 4326)), ST_y(ST_Transform( e.location, 4326)),e.address_event 
                FROM events e
                WHERE e.id_event = {}
            '''.format(id_event)
            cur.execute(select)
            row = cur.fetchone()
            if row[2] == 0:
                var = '''{} (прочее)'''.format(row[1])
            else:
                var = row[1]
            text = '''
\t\t*{}*
_тип события_: {}
_когда_: {} в {}
_адрес_: {}
_описание_: ``` {} ```
*создал:* _{}_
*рейтинг*: {}
            '''.format(row[0],
                       var,
                       row[4],
                       ':'.join(str(row[3]).split(':')[:-1]),
                       row[9],
                       row[5],
                       get_user(row[6], cur),
                       get_range_event_user(row[6], cur)
                       )
            conn.close()
            return 0, text, [row[7], row[8], is_type_us_to_ev, is_check]
        return -1, '', []
    return -2, '', []


def get_event_today_user_go_maybe_go(id_user):
    conn = create_connection()
    date = datetime.datetime.now()
    if conn:
        cur = conn.cursor()
        select = '''
            SELECT e.id_event, e.title_event, e.time_event,e.text_event_type
            FROM events e, user_to_event ue
            WHERE ue.ref_id_user = {}
            AND ue.ref_id_event = e.id_event
            AND (ue.ref_user_to_event_type = 1 or ue.ref_user_to_event_type = 2)
            AND e.date_event = '{}'
        '''.format(id_user, '{}-{}-{}'.format(date.month, date.day, date.year))
        cur.execute(select)
        rows = cur.fetchall()
        res = []
        conn.close()
        for r in rows:
            s = '{}. {} в {} ({})'.format(r[0], r[1], r[2], r[3])
            res.append(s)
        return res


def get_my_actual_event(id_user):
    conn = create_connection()
    date = datetime.datetime.now()
    if conn:
        cur = conn.cursor()
        select = '''
                SELECT e.id_event, e.title_event, e.time_event,e.text_event_type
                FROM events e
                WHERE e.date_event >= '{}'
                AND e.ref_id_users = {}
            '''.format('{}-{}-{}'.format(date.month, date.day, date.year),
                       get_id_user_by_chat_id(id_user, cur))
        cur.execute(select)
        # print(select)
        res = ['{}. {} в {} ({})'.format(r[0], r[1], r[2], r[3]) for r in cur.fetchall()]
        conn.close()
        return res


def get_event_user(id_event,
                   id_user):
    conn = create_connection()
    if conn:
        cur = conn.cursor()

        if is_exist_event(id_event, cur):
            select = '''
                SELECT e.title_event, e.text_event_type, e.ref_id_type_event, e.time_event, e.date_event,
                e.description, e.ref_id_users,
                ST_x(ST_Transform( e.location, 4326)), ST_y(ST_Transform( e.location, 4326)),e.address_event 
                FROM events e
                WHERE e.id_event = {} AND e.ref_id_users = {}
            '''.format(id_event,get_id_user_by_chat_id(id_user,cur))
            cur.execute(select)
            row = cur.fetchone()
            if row:
                if row[2] == 0:
                    var = '''{} (прочее)'''.format(row[1])
                else:
                    var = row[1]
                text = '''
\t\t*{}*
_тип события_: {}
_когда_: {} в {}
_адрес_: {}
_описание_: ``` {} ```

*создал:* _{}_
*рейтинг*: {}
                '''.format(row[0],
                           var,
                           row[4],
                           ':'.join(str(row[3]).split(':')[:-1]),
                           row[9],
                           row[5],
                           get_user(row[6], cur),
                           get_range_event_user(row[6], cur)
                           )
                conn.close()
                return 0, text, [row[7], row[8]]
            else:
                return -3, '', []
        return -1, '', []
    return -2, '', []


def remove_event_user(id_event,
                      id_user):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select = '''
                DELETE FROM user_to_event WHERE ref_id_event = {};
                DELETE FROM events WHERE ref_id_users = {} AND id_event = {};
            '''.format(id_event,
                       get_id_user_by_chat_id(id_user,
                                              cur),
                       id_event)
            cur.execute(select)
            conn.commit()
            return 0
        except:
            return -1
    return -2


def set_type_user_to_event(id_event, id_user, type_):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select = '''
                INSERT INTO user_to_event (id_user_to_event, ref_id_user, ref_id_event, ref_user_to_event_type) 
                VALUES (DEFAULT, {}, {}, {}) 
            '''.format(get_id_user_by_chat_id(id_user, cur),
                       id_event,
                       type_)
            cur.execute(select)
            conn.commit()
            conn.close()
            return 0
        except:
            conn.close()
            return -2

    return -1


def check_in_user(id_user, id_event, lat, lng):
    try:
        conn = create_connection()
        if conn:
            cur = conn.cursor()
            select = '''
                SELECT e.id_event, ST_x(ST_Transform( e.location, 4326)), ST_y(ST_Transform( e.location, 4326))
                FROM events e
                WHERE e.id_event = {}
            '''.format(id_event)
            cur.execute(select)
            row = cur.fetchone()
            if row:
                select = '''SELECT ST_Distance( 'SRID=4326;POINT({} {})'::geography,'SRID=4326;POINT({} {})'::geography)'''.format(
                    row[1],
                    row[2],
                    lat,
                    lng)
                # print(select)
                cur.execute(select)
                length = float(cur.fetchone()[0])
                # print(length)
                if length < 50:
                    id_us = get_id_user_by_chat_id(id_user, cur)
                    try:
                        ins = '''
                                                    INSERT INTO user_to_event (id_user_to_event, ref_id_user, ref_id_event, ref_user_to_event_type) 
                                                    VALUES (DEFAULT , {}, {}, 4) 
                                                '''.format(id_us, id_event)
                        cur.execute(ins)

                        conn.commit()
                        conn.close()
                        return 0

                    except:
                        traceback.print_exc()
                        conn.close()
                        conn = create_connection()
                        cur = conn.cursor()
                        up = '''
                                                    UPDATE user_to_event
                                                    Set ref_user_to_event_type = 4
                                                    WHERE ref_id_event = {}
                                                    AND ref_id_user = {}
                                                    '''.format(id_event, id_us)
                        cur.execute(up)
                        conn.commit()
                        conn.close()
                        return 0
                return -2
            return -1
    except:
        traceback.print_exc()
        pass


def set_opinion(id_event, id_user, opinion):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        select = '''
            SELECT e.ref_id_users, ue.id_user_to_event
            FROM events e, user_to_event ue
            WHERE ue.ref_id_user = {}
            AND ue.ref_id_event = e.id_event
            AND e.id_event = {}
        '''.format(get_id_user_by_chat_id(id_user, cur), id_event)
        cur.execute(select)
        row = cur.fetchone()
        # print(row)
        insert = '''
            INSERT INTO user_to_event_opinion (id_user_to_event_opinion, opinion, ref_id_user_to_event) 
            VALUES (DEFAULT , {}, {}) 
        '''.format(opinion, row[1])
        # print(select)
        cur.execute(insert)
        conn.commit()
        conn.close()


def get_inf_user(id_user):
    conn = create_connection()
    cur = conn.cursor()
    reiting  = get_range_event_user(get_id_user_by_chat_id(id_user, cur),cur)
    conn.close()
    return 'ваш рейтинг: {}'.format(reiting)