import telebot as tb
from db.redis_ import set_last_step_user,\
    set_id_current_event
from messages import message_opinion_user
from db.__init__ import get_event
from emoji import one,\
    two,\
    three,\
    four,\
    five
from config import token,\
    path_celery
import time
from celery import Celery
app = Celery('evenot', broker=path_celery)

bot = tb.TeleBot(token)


@app.task
def send_data(id_event,
              id_user):
    # print(15)
    time.sleep(5)
    set_last_step_user(id_user, -1)
    set_id_current_event(id_user, id_event)
    bot.send_message(id_user,
                     message_opinion_user)
    code, text, location = get_event(id_event,
                                     id_user)
    keyboard = tb.types.ReplyKeyboardMarkup(False, True, True)
    keyboard.row(one,
                 two,
                 three,
                 four,
                 five)
    bot.send_message(id_user,
                     text,
                     parse_mode='Markdown',
                     reply_markup=keyboard)
    bot.send_location(id_user, location[0], location[1])
